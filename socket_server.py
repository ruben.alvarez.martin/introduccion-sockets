import socket

# Creamos un objeto de socket
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Obtenemos el nombre de host del servidor
host = socket.gethostname()

# Especificamos el puerto en el que escuchará el servidor
port = 12346

# Ligamos el socket al host y puerto
serversocket.bind((host, port))

# Empezamos a esperar conexiones
serversocket.listen(5)

print("Esperando conexiones...")

while True:
    # Aceptamos la conexión
    clientsocket, addr = serversocket.accept()

    print("Conexión desde: %s" % str(addr))

    msg = '¡Hola, cliente! Gracias por conectarte\n'
    clientsocket.send(msg.encode('utf-8'))

    # Cerramos la conexión con el cliente
    clientsocket.close()