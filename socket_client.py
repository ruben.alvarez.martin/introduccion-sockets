import socket

# Creamos un objeto de socket
clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Obtenemos el nombre de host del servidor
host = socket.gethostname()

# Especificamos el puerto en el que el servidor está escuchando
port = 12346

# Nos conectamos al servidor
clientsocket.connect((host, port))

# Recibimos el mensaje del servidor
msg = clientsocket.recv(1024)

print(msg.decode('utf-8'))

# Cerramos la conexión con el servidor
clientsocket.close()